section .text
%include "lib.inc"
global find_word
global print_dict


find_word:
    .loop:
    	push rsi
    	add rsi, 8 
    	call string_equals
    	pop rsi
    	cmp rax, 0 
    	jne .found 
    	mov rsi, [rsi] 
    	cmp rsi, 0 
    	je .not_found
    	jmp .loop 
    .found:
        mov rax, rsi 
        ret
    .not_found:
        xor rax, rax
        ret	

       
print_dict:

    .loop:
        push rsi            
        push rdi            
        add rdi, 8   
        call print_string
        call print_newline
        pop rdi            
        pop rsi            
        mov rdi, [rdi]    
        test rdi, rdi
        jnz .loop         
        xor rax, rax      
    ret  
