section .text
    
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 
    syscall 
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax 
    .loop: 
        cmp byte[rax+rdi],0 
        je .end
        inc rax 
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в поток вывода
print_string:
    xor rax, rax
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    mov rdx, rax 
    push rsi
    mov rsi, rdi 
    pop rdi
    mov rax, 1 
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char: 
    push rdi 
    mov rdx, 1     
    mov rsi, rsp 
    pop rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax 
    mov r8, 10
    mov r9, rsp
    mov rax, rdi

    push 0
    .loop:
        xor rdx,rdx
        div r8 
        add rdx, '0'
        dec rsp 
        mov byte[rsp], dl 
        cmp rax,0  
        je .end
        jmp .loop 

    .end:
        mov rdi, rsp 
        push r9
        call print_string
        pop r9
        mov rsp, r9 
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .neg 
    jmp print_uint 

    .neg:
        push rdi  
        mov rdi, '-' 
        call print_char
        pop rdi
        neg rdi 
        jmp print_uint 

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    xor r8, r8 
    xor r9, r9
    .loop: 
        mov r8b, byte[rdi+rcx] 
        mov r9b, byte[rsi+rcx] 
        cmp r8b,r9b 
        jne .no 
        cmp r8b,0
        je .yes 
        inc rcx 
        jmp .loop
    
    .yes:
        mov rax, 1   
        ret
    .no:
        mov rax,0 
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax 
    xor rdi,rdi 
    mov rdx, 1 
    push 0 
    mov rsi, rsp 
    syscall
    pop rax 
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor rax, rax
    
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char 
        pop rdi
        pop rsi
        pop rcx

        cmp rax, 0 
        je .end

        cmp rax, ' ' 
        je .skip_whitespace
        cmp rax, '	'
        je .skip_whitespace
        cmp rax, 0xA
        je .skip_whitespace

        mov [rdi+rcx], rax 
        inc rcx 
        cmp rcx, rsi 
        jge .error

        jmp .loop

    .skip_whitespace:
        cmp rcx,0 
        je .loop
        jmp .end 
    .error:                                                                
        xor rax, rax 
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax
        mov rax, rdi 
        mov rdx, rcx 
        ret
    




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 
    .loop: 
        movzx r9, byte[rdi+rcx] 
        cmp r9,0 
        je .end
        cmp r9b, '0'
        jl .end     
        cmp r9b, '9'
        jg .end

        mul r8 
        sub r9b, '0'
        add rax, r9 
        inc rcx 
        jmp .loop

    .end:
        mov rdx, rcx 
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi 
    cmp byte[rcx], '-'
    je .neg
    jmp .pos 
    .neg:
        inc rcx 
        mov rdi, rcx
        push rcx
        call parse_uint 
        pop rcx
        neg rax 
        inc rdx 
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint 
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy: 
    xor rax, rax
    xor rcx, rcx 
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r8, rax 
    cmp rdx, r8 
    jl .error
    .loop:
        cmp rcx, r8
        jg .end
        mov r10,[rdi+rcx] 
        mov [rsi+rcx], r10
        inc rcx
        jmp .loop
    
    .error:
        mov rax, 0 
        ret
    .end:
        mov rax, r8
        ret
