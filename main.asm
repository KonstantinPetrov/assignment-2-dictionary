%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2
%define OFFSET 8

section .data
%include "word.inc"
start_msg:
    db 'Enter the key: ', 0
err_msg: 
    db 'Couldn not read the word', 10, 0
not_found_msg: 
    db 'The key was not found in the dictionary', 10, 0
found_msg: 
    db 'Value by key: ', 0

section .text
extern find_word
extern print_dict
%include "lib.inc"

global _start
_start:

    mov rdi, start_msg
    mov rsi, STDOUT     ;first message
    call print_string

    sub rsp, BUFFER_SIZE ;allocate memory on the stack
    mov rsi, BUFFER_SIZE ;set the maximum input length for the read_word function
    mov rdi, rsp
    call read_word
    cmp rax, 0 ;error
    jz .err_read ;error enter
    mov rsi, first ;rsi points to the first key-value pair
    mov rdi, rax ;in rdi- entered word
    call find_word 
    cmp rax, 0  ;tried to find, if not then go to .not_found

    je .not_found

    .found:
        add rax, OFFSET ;offset to the address of the beginning of the key
        push rax
        mov rsi, STDOUT
        mov rdi, found_msg
        call print_string ;we display a message that a string was found, then we need to display the string itself

        ;the couple is made up of key_string \0 value_string \0
        ;we need value_string
        pop rax ;in rax - key address
        mov rdi, rax
        call string_length
        inc rax
        add rdi, rax ;rdi = addr + key_len + 1(null-terminator)
        mov rsi, STDOUT
        call print_string ;enter value_string
        jmp .end

    .err_read:
        mov rdi, err_msg
        mov rsi, STDERR ;display an input error message
        call print_string
        jmp .end

    .not_found:
        mov rdi, not_found_msg
        mov rsi, STDERR ;display an input error message
        call print_string
        jmp .end

    .end:
        add rsp, BUFFER_SIZE ;restore stack pointer
        call exit
	
